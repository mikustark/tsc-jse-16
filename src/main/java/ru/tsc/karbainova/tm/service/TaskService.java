package ru.tsc.karbainova.tm.service;

import ru.tsc.karbainova.tm.api.repository.ITaskRepository;
import ru.tsc.karbainova.tm.api.service.ITaskService;
import ru.tsc.karbainova.tm.enumerated.Status;
import ru.tsc.karbainova.tm.exception.empty.EmptyIdException;
import ru.tsc.karbainova.tm.exception.empty.EmptyIndexException;
import ru.tsc.karbainova.tm.exception.empty.EmptyNameException;
import ru.tsc.karbainova.tm.exception.entity.TaskNotFoundException;
import ru.tsc.karbainova.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public void create(String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = new Task();
        task.setName(name);
    }

    @Override
    public void create(String name, String description) {
        if (name == null || name.isEmpty()) return;
        if (description == null || description.isEmpty()) return;
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        taskRepository.add(task);
    }

    @Override
    public void add(Task task) {
        if (task == null) throw new TaskNotFoundException();
        taskRepository.add(task);
    }

    @Override
    public void remove(Task task) {
        if (task == null) throw new TaskNotFoundException();
        taskRepository.remove(task);
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public List<Task> findAll(Comparator<Task> comparator) {
        return taskRepository.findAll(comparator);
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }

    @Override
    public Task updateById(String id, String name, String description) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = taskRepository.findById(id);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateByIndex(Integer index, String name, String description) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = taskRepository.findByIndex(index);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task removeById(String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return taskRepository.removeById(id);
    }

    @Override
    public Task removeByIndex(Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        return taskRepository.removeByIndex(index);
    }

    @Override
    public Task removeByName(String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.removeByName(name);
    }

    @Override
    public Task findById(String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return taskRepository.findById(id);
    }

    @Override
    public Task findByIndex(Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        return taskRepository.findByIndex(index);
    }

    @Override
    public Task findByName(String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.findByName(name);
    }

    @Override
    public Task startById(String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Task task = taskRepository.findById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startByIndex(Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        final Task task = taskRepository.findByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startByName(String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = taskRepository.findByName(name);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task finishById(String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Task task = taskRepository.findById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.COMPLETE);
        return task;
    }

    @Override
    public Task finishByIndex(Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        final Task task = taskRepository.findByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.COMPLETE);
        return task;
    }

    @Override
    public Task finishByName(String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = taskRepository.findByName(name);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.COMPLETE);
        return task;
    }
}
